# Exams (`adnexam.cls`)

The class `adnexam.cls` creates exams with open questions and answers by extending the [`exam.cls`](http://ctan.org/tex-archive/macros/latex/contrib/exam/) functionalities.

```latex
\documentclass{adnexam}
% Uncomment to generate answers
%\printanswers

\setlogo{UNICAMP}

\coursecode{IT-1000}
\instructors{Prof. Mengano}
\title{Exam of Course}

% We can set where to put the points
%\pointsinmargin
\pointsinrightmargin

\begin{document}
\maketitle

\begin{questions}

\question[50]
Show that $P = NP$.

\begin{parts}
\part[35]
Through induction.
\part[35]
Through direct proof.

\begin{solution}
Solution is trivial (it is left as an exercise to the reader).
\end{solution}
\end{parts}

\question[50]
Solve equations.

\begin{solution}
Solution is trivial (it is left as an exercise to the reader).
\end{solution}

\end{questions}

\end{document}
```

